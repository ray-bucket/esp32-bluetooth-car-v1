#define CUSTOM_SETTINGS
#define INCLUDE_GAMEPAD_MODULE
#define INCLUDE_SENSOR_MODULE

#include <DabbleESP32.h>
#include <Arduino.h>

enum Wheel { Left, Right };
enum Direction { Front, Back, Off };

// https://uelectronics.com/producto/devkitv1-esp32-modulo-wifi-bluetooth-esp32-arduino/
const int CTRL_L1 = 26;
const int CTRL_L2 = 25;
const int CTRL_R1 = 33;
const int CTRL_R2 = 32;
const int REVERSE_LED = 27;

void move(Wheel wheel, Direction direction) {
  int ctrl1 = wheel == Wheel::Left ? CTRL_L1 : CTRL_R1;
  int ctrl2 = wheel == Wheel::Left ? CTRL_L2 : CTRL_R2;

  if (direction == Direction::Front) {
    // Serial.println(String("Front ") + ctrl1 + " " + ctrl2);
    digitalWrite(ctrl1, 1);
    digitalWrite(ctrl2, 0);
  } else if (direction == Direction::Back) {
    // Serial.println(String("Back ") + ctrl1 + " " + ctrl2);
    digitalWrite(ctrl1, 0);
    digitalWrite(ctrl2, 1);
  } else {
    // Serial.println(String("Stop ") + ctrl1 + " " + ctrl2);
    digitalWrite(ctrl1, 0);
    digitalWrite(ctrl2, 0);
  }
}

void setup() {
  Serial.begin(115200);
  Dabble.begin("Car v1");

  pinMode(CTRL_L1, OUTPUT);
  pinMode(CTRL_L2, OUTPUT);
  pinMode(CTRL_R1, OUTPUT);
  pinMode(CTRL_R2, OUTPUT);
  pinMode(REVERSE_LED, OUTPUT);
}

void loop() {
  Dabble.processInput();
  if (!Dabble.isAppConnected()) return;

  if (GamePad.isUpPressed()) {
    move(Wheel::Left, Direction::Front);
    move(Wheel::Right, Direction::Front);
  } else if (GamePad.isDownPressed()) {
    move(Wheel::Left, Direction::Back);
    move(Wheel::Right, Direction::Back);
    digitalWrite(REVERSE_LED, 1);
  } else if(GamePad.isLeftPressed()) {
    move(Wheel::Left, Direction::Front);
    move(Wheel::Right, Direction::Back);
  } else if (GamePad.isRightPressed()) {
    move(Wheel::Left, Direction::Back);
    move(Wheel::Right, Direction::Front);
  } else {
    digitalWrite(REVERSE_LED, 0);
    move(Wheel::Left, Direction::Off);
    move(Wheel::Right, Direction::Off);
  }
}
